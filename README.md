# ATAI valid_move task

## How to run

### Metalog

```bash
cd metalog
$ swipl valid_move.pl
```

```pl
?- time(learn).
```

### Poppper

```bash
cd Popper
# Install the dependencies as shown in Popper/README.md
$ python3 popper.py examples/valid_move --stats
```

### Hyper

```bash
cd hyper
$ swipl adjacent.pl hyper.pl util.pl
```

```pl
?- time(learn).
```
