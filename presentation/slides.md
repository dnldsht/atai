---
theme: seriph
background: https://source.unsplash.com/collection/94734566/1920x1080
class: text-center
highlighter: shiki
lineNumbers: false
info: |
  ## Slidev Starter Template
  Presentation slides for developers.

  Learn more at [Sli.dev](https://sli.dev)
drawings:
  persist: false
title: Welcome to Slidev
---

# Advanced topics in Artificial Intelligence 2022

Donald Shtjefni

<div class="pt-12">
  <span @click="$slidev.nav.next" class="px-2 py-1 rounded cursor-pointer" hover="bg-white bg-opacity-10">
    Press Space for next page <carbon:arrow-right class="inline"/>
  </span>
</div>

<!--
The last comment block of each slide will be treated as slide notes. It will be visible and editable in Presenter Mode along with the slide. [Read more in the docs](https://sli.dev/guide/syntax.html#notes)
-->

---

# Tools

<h2 class="mt-6">Metagol</h2> 
<small>2016 - Andrew Cropper and Stephen H. Muggleton</small>

<h2 class="mt-6">Popper</h2>
<small>2021 - Andrew Cropper and Rolf Morel</small>

<h2 class="mt-6">Hyper <small>(Bonus)</small> </h2>
<small>1999 - Ivan Bratko</small>

---

# Goals

<h2 class="mt-6 mb-4">Learnign with ILP</h2>

- Learn what is a valid move (obstacles)
- Learn the adajcent clause

<h2 class="mt-6 mb-4">Compare different tools</h2>

- Performance
- Traits

---

# Metagol

4 components:

- **Metarules**
- **Background Knowledge**
- **Positive Examples**
- **Negative Examples**

Learning process:

- Select a positive example to be proven.
- Prove it through either the existing background knowledge or clauses previously induced.
- (If step 2 did not work) Unify the example with the head of a metarule and repeat the process for each of the atoms in the body.
- Once the hypothesis is complete, check its consistency. If negative examples are covered then backtrack

---

# The grid

```prolog
% defines grid NxM
grid(5,5).

%   | 1 | 2 | 3 | 4 | 5 |
%   ---------------------
% 1 |   |   |   |   | x |
% 2 | x |   | x |   |   |
% 3 |   |   |   |   |   |
% 4 | x | x | x | x |   |
% 5 | x |   |   |   |   |

% obstacles
obstacle(cell(5,1)).
obstacle(cell(1,2)).
% ...
obstacle(cell(4,4)).
obstacle(cell(1,5)).
```

---

# Learning `valid_move/2`

Background Knowledge

```prolog
safe_succ(A,B) :- integer(A), succ(A,B).
safe_succ(A,B) :- integer(B), succ(A,B).

adjacent(cell(X,Y),cell(X,Y1)) :-
    safe_succ(Y1, Y).

adjacent(cell(X,Y),cell(X1,Y)) :-
    safe_succ(X, X1).

inside_grid(cell(X,Y)) :-
    integer(X), integer(Y),
    grid(Width,Height),
    X > 0,
    X =< Width,
    Y > 0,
    Y =< Height.

is_free(CELL) :- \+ obstacle(CELL).

legal_position(CELL) :-
    inside_grid(CELL),
    is_free(CELL).
```

---

# Learning `valid_move/2`

Bias

```prolog
%% metagol settings
%metagol:max_clauses(4).

%% tell metagol to use the BK
body_pred(adjacent/2).
body_pred(legal_position/1).

%% metarules
% metarule([P,Q], [P,A,B], [[Q,A,B]]). % identity
% metarule([P,Q], [P,A,B], [[Q,B,A]]). % inverse
% metarule([P,Q,R], [P,A,B], [[Q,A],[R,A,B]]). % precon
metarule([P,Q,R], [P,A,B], [[Q,A,B],[R,B]]). % postcon
% metarule([P,Q,R], [P,A,B], [[Q,A,C],[R,C,B]]). % chain
metarule([P,Q,R], [P,A,B], [[Q,B,A],[R,B]]). % inverse + postcon
```

Result:

```prolog
valid_move(A,B):-adjacent(A,B),legal_position(B).
valid_move(A,B):-adjacent(B,A),legal_position(B).
```

---

# Learning `valid_move/2`

## with `inside_grid/1` and `is_free/1`

Metarules

```prolog
metarule([P,Q,R,S], [P,A,B], [[Q,A,B],[R,B],[S,B]]). % double post-con
metarule([P,Q,R,S], [P,A,B], [[Q,B,A],[R,B],[S,B]]). % inverse double post-con

?- time(a).
% learning valid_move/2
% clauses: 1
% clauses: 2

valid_move(A,B) :-
  adjacent(A,B),
  inside_grid(B),
  is_free(B).

valid_move(A,B) :-
  adjacent(B,A),
  inside_grid(B),
  is_free(B).
```

---

# Popper

3 files components:

- an examples file (positive & negative)
- a background knowledge (BK) file
- a bias file

The bias file defines predicate declarations, which tell Popper the
predicate symbols to use in the head or body of a rule.

- `head pred(p,k)` states that the predicate p with arity k can be used
  in the head of the learned rules.
- `body_pred(p,k)` states the same for the body of the rule.
- Optionaly we can define the types and the direction of the predicates.

---

# Learning `valid_move/2`

Examples

```prolog
pos(valid_move(cell(3,1), cell(4,1))). % right
pos(valid_move(cell(2,2), cell(2,3))). % down
pos(valid_move(cell(4,3), cell(3,3))). % left
pos(valid_move(cell(2,2), cell(2,1))).  % up
neg(valid_move(cell(4,1), cell(5,1))). % right
neg(valid_move(cell(4,3), cell(4,4))). % down
neg(valid_move(cell(1,1), cell(0,1))). % left
neg(valid_move(cell(5,2), cell(5,1))). % up
```

Bias

```prolog
max_vars(2).

head_pred(valid_move,2).
body_pred(adjacent,2).
body_pred(legal_position,1).
```

---

# Learning `valid_move/2`

Bias

```prolog
type(adjacent,(cell, cell)).
type(valid_move,(cell, cell)).
type(legal_position,(cell,)).

direction(adjacent,(out,out)).
direction(valid_move,(in,out)).
direction(legal_position,(in,)).
```

Result

```prolog
% Precision:1.00 Recall:1.00 TP:4 FN:0 TN:5 FP:0 Size:6

valid_move(A,B) :-
  adjacent(B,A),
  legal_position(B).

valid_move(A,B) :-
  adjacent(A,B),
  legal_position(B).
```

---

# Learning `valid_move/2`

## with `inside_grid/1` and `is_free/1`

Bias

```prolog
head_pred(valid_move,2).
body_pred(adjacent,2).
body_pred(is_free,1).
body_pred(inside_grid,1).

type(adjacent,(cell, cell)).
type(valid_move,(cell, cell)).
type(is_free,(cell,)).
type(inside_grid,(cell,)).

direction(adjacent,(out,out)).
direction(valid_move,(in,out)).
direction(is_free,(in,)).
direction(inside_grid,(in,)).
```

---

# Learning `valid_move/2`

## with `inside_grid/1` and `is_free/1`

Examples

```prolog
% added the folowing negative example to cover the case where the cell is outside the grid
neg(valid_move(cell(5,5), cell(6,5))).  % up
```

Result

```prolog
% Precision:1.00 Recall:1.00 TP:4 FN:0 TN:5 FP:0 Size:8

valid_move(A,B) :-
  adjacent(B,A),
  inside_grid(B),
  is_free(B).

valid_move(A,B) :-
  adjacent(A,B),
  inside_grid(B),
  is_free(B).
```

---

# Bonus `adjacent/2` in Hyper

```prolog
% Parameter settings
max_proof_length( 2).
max_clauses(2).
max_clause_length(2).

domain(L) :- numlist(1,5,L).
safe_succ(X,Y):- domain(L), member(X, L), member(Y, L), (Y is X+1; Y is X-1).

% Background literals
backliteral(safe_succ(A, B), [A:integer], [ B:integer]).

% Refinement of terms
term( cell, c(X, Y), [X:integer, Y:integer]).

% background predicates
prolog_predicate(safe_succ(_,_)).

start_clause( [adjacent( C1, C2)] / [ C1:cell, C2:cell] ).

% ... 4 positive examples and 3 negative examples
```

---

# Bonus `adjacent/2` in Hyper

Examples

```prolog
ex( adjacent( c(3,3), c(2,3))). % left
ex( adjacent( c(4,5), c(4,4))). % up
ex( adjacent( c(3,1), c(4,1))). % right
ex( adjacent( c(1,3), c(1,4))). % down


nex( adjacent( c(1,2), c(1,2))).
nex( adjacent( c(3,3), c(2,4))).
nex( adjacent( c(3,3), c(3,1))).
```

Result

```prolog
adjacent(c(A,B),c(A,C)):-
  safe_succ(B,C).

adjacent(c(A,B),c(C,B)):-
  safe_succ(A,C).
```

---

# Perfromance

All the times are in seconds, an Imac with M1 processor has been used for the tests.

All the solution has precision 1.00 and recall 1.00.

<div class="my-4" />

|              | Metagol | Popper            | Hyper |
| ------------ | ------- | ----------------- | ----- |
| valid_move   | 0.013   | op 0.00 / ex 0.03 | NA    |
| valid_move\* | 0.015   | op 0.01 / ex 0.03 | NA    |
| adjacent     | NA      | NA                | 1.804 |

(\*) referes to `valid_move/2` with `inside_grid/1` and `is_free/1`

---

# Metagol

<h2>Pros</h2>
<div class="pl-2 mt-4">

<h4>Fast</h4>
<h4>Predicate invention</h4>
<h4>Easy to use</h4>

</div>

<h2 class="mt-6">Cons</h2>
<div class="pl-2 mt-4">

<h4>Metarules</h4>
<small>You need a precise idea of the solution to seelct the correct metarules.</small>
<h4>Predicates arities</h4>
<small>Becomes slow with predicates of arities greater than two</small>
</div>

<style>
  h4{
    margin-top:.5rem;
  }
</style>

---

# Popper

<h2>Pros</h2>
<div class="pl-2 mt-4">

<h4>Fast</h4>
<h4>Doesn't require metarules</h4>
<h4>Easy to use</h4>

</div>

<h2 class="mt-6">Cons</h2>
<div class="pl-2 mt-4">

<h4>Examples</h4>
<small>You need to provide a really curated set of examples.</small>
<h4>Hyper parameters</h4>
<small>You need to tune the parameters during the learning</small>
</div>

<style>
  h4{
    margin-top:.5rem;
  }
</style>

---

# Hyper

<h2>Pros</h2>
<div class="pl-2 mt-4">

<h4>Doesn't require metarules</h4>
<h4>Term refinements</h4>
<h4>Great support for `backliterals`</h4>

</div>

<h2 class="mt-6">Cons</h2>
<div class="pl-2 mt-4">

<h4>Not so fast</h4>
<h4>Examples</h4>
<small>You need to provide a really curated set of examples.</small>
<h4>Hyper parameters</h4>
<small>You need to tune the parameters during the learning</small>
<h4>Lack of documentation</h4>
<small>You need to tune the parameters during the learning</small>
</div>

<style>
  h4{
    margin-top:.5rem;
  }
</style>
