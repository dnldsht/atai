%% learning task
pos(valid_move(cell(3,1), cell(4,1))). % right
pos(valid_move(cell(2,2), cell(2,3))). % down
pos(valid_move(cell(4,3), cell(3,3))). % left
pos(valid_move(cell(2,2), cell(2,1))).  % up

neg(valid_move(cell(4,1), cell(5,1))). % right 
neg(valid_move(cell(4,3), cell(4,4))). % down
neg(valid_move(cell(1,1), cell(0,1))). % left
neg(valid_move(cell(5,2), cell(5,1))).  % up
neg(valid_move(cell(5,5), cell(6,5))).  % up

%neg(valid_move(cell(1,1), cell(1,1))).  % not same