% up
pos(adjacent(2,4,2,3)).
pos(adjacent(1,6,1,5)).
pos(adjacent(4,6,4,5)).

% down
pos(adjacent(3,2,3,3)).
pos(adjacent(1,5,1,6)).
pos(adjacent(1,1,1,2)).

% left
pos(adjacent(2,3,3,3)).
pos(adjacent(4,5,5,5)).
pos(adjacent(5,2,6,2)).

% right
pos(adjacent(6,2,5,2)).

% up
neg(adjacent(2,5,2,4)).
neg(adjacent(1,2,1,2)).

% down
neg(adjacent(1,2,1,3)).
neg(adjacent(4,3,4,4)).

% left
neg(adjacent(2,4,3,4)).
neg(adjacent(3,4,4,4)).
