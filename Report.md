# Learning what is valid move (Adajcents cells + obstacles)

Donald Shtjefni - Advanced Topics in Artificial Intelligence 2022

## Introduction

The aim of this project is to use different Inductive Logic Tools on the same task, to compare the results.

I will focus more on performances and traits of the used tools.

## Tools

### Metagol

Metagol is an inductive logic programming (ILP) system based on meta-interpretive learning.

Metalog need the following settings

- **Metarules** are used to define the language bias of the task. A large number metarules allows for a less strict language bias, hence a larger search space in which to find a solution.
- **Background Knowledge** It is a set of Prolog rules that the system can use either directly or indirectly in order to induce the hypothesis.
- **Positive Examples**
- **Negative Examples**

Providing these 4 components, Metagol will try to find a solution running the following algorithm:

- Select a positive example to generalise. If none exists, stop.
- Try to prove the example (an atom) by:
  - using given BK or a clause that is already induced
  - unifying the atom with the head of a metarule, saving the substitutions,
    and then proving the body of the metarule through meta-interpretation
    (by treating the body atoms as examples and applying step 2 to them).
- After proving all the positive examples, check the hypothesis against
  the negative examples. If the hypothesis does not entail any negative
  example stop; otherwise backtrack to a choice point at step 2 and
  continue.

### Popper

Popper is an inductive logic programming (ILP) system.

Popper requires three files:

- an examples file (positive & negative)
- a background knowledge (BK) file
- a bias file

The bias file defines predicate declarations, which tell Popper the
predicate symbols to use in the head or body of a rule.

- `head pred(p,k)` states that the predicate p with arity k can be used
  in the head of the learned rules.
- `body_pred(p,k)` states the same for the body of the rule.
- Optionaly we can define the types and the direction of the predicates.

## Implementation

### Metalog

#### Defining the grid

In the background knowledge we defined the dimension of the grid and where the obstacles are located.

```prolog
% defines grid NxM
grid(5,5).

%   | 1 | 2 | 3 | 4 | 5 |
%   ---------------------
% 1 |   |   |   |   | x |
% 2 | x |   | x |   |   |
% 3 |   |   |   |   |   |
% 4 | x | x | x | x |   |
% 5 | x |   |   |   |   |

% obstacles
obstacle(cell(5,1)).
obstacle(cell(1,2)).
% ...
obstacle(cell(4,4)).
obstacle(cell(1,5)).
```

#### Learning valid_move

To lean what is a valid move I defined the following predicates:

```prolog
safe_succ(A,B) :- integer(A), succ(A,B).
safe_succ(A,B) :- integer(B), succ(A,B).

adjacent(cell(X,Y),cell(X,Y1)) :-
    safe_succ(Y1, Y).

adjacent(cell(X,Y),cell(X1,Y)) :-
    safe_succ(X, X1).

inside_grid(cell(X,Y)) :-
    integer(X), integer(Y),
    grid(Width,Height),
    X > 0,
    X =< Width,
    Y > 0,
    Y =< Height.

is_free(CELL) :- \+ obstacle(CELL).

legal_position(CELL) :-
    inside_grid(CELL),
    is_free(CELL).
```

The bias settings looks like this:

```prolog
%% metagol settings
%metagol:max_clauses(4).

%% tell metagol to use the BK
body_pred(adjacent/2).
body_pred(legal_position/1).


%% metarules
metarule([P,Q], [P,A,B], [[Q,A,B]]). % identity
metarule([P,Q], [P,A,B], [[Q,B,A]]). % inverse
% metarule([P,Q,R], [P,A,B], [[Q,A],[R,A,B]]). % precon
metarule([P,Q,R], [P,A,B], [[Q,A,B],[R,B]]). % postcon
% metarule([P,Q,R], [P,A,B], [[Q,A,C],[R,C,B]]). % chain
```

With `body_pred` I tell metagol which predicates can be used in the body of the learned rules.

For the meta rules I started with all the rules that metagol suggested exept the recursive one.

```prolog
metarule([P,Q], [P,A,B], [[Q,A,B]]). % identity
metarule([P,Q], [P,A,B], [[Q,B,A]]). % inverse
metarule([P,Q,R], [P,A,B], [[Q,A],[R,A,B]]). % precon
metarule([P,Q,R], [P,A,B], [[Q,A,B],[R,B]]). % postcon
metarule([P,Q,R], [P,A,B], [[Q,A,C],[R,C,B]]). % chain
```

Looking a the solution provided, I refined the rules, keeping only the relevant ones.

This is what ILP learned:

```prolog
?- time(a).
% learning valid_move/2
% clauses: 1
% clauses: 2
% clauses: 3
valid_move(A,B) :-
  adjacent(A,B),
  legal_position(B).

valid_move(A,B) :-
  valid_move_1(A,B),
  legal_position(B).

valid_move_1(A,B) :-
  adjacent(B,A).
% 24,184 inferences, 0.005 CPU in 0.005 seconds (89% CPU, 5129162 Lips)
```

To improve the generated clauses, I've added the "inverse + post condition" metarule.

```prolog
metarule([P,Q,R], [P,A,B], [[Q,B,A],[R,B]]). % inverse + postcon
```

Now the result is:

```prolog
?- time(a).
% learning valid_move/2
% clauses: 1
% clauses: 2
valid_move(A,B):-adjacent(A,B),legal_position(B).
valid_move(A,B):-adjacent(B,A),legal_position(B).
% 53,008 inferences, 0.012 CPU in 0.013 seconds (90% CPU, 4368911 Lips)
```

### Popper

#### Background knowledge

The Background knowledge used in Popper is the same as in Metagol, it includes the definition of the grid, `legal_position/2` and `adjacent/2`.

#### Learning valid_move

To learn what is a valid move I provided the following examples:

```prolog
pos(valid_move(cell(3,1), cell(4,1))). % right
pos(valid_move(cell(2,2), cell(2,3))). % down
pos(valid_move(cell(4,3), cell(3,3))). % left
pos(valid_move(cell(2,2), cell(2,1))).  % up

neg(valid_move(cell(4,1), cell(5,1))). % right
neg(valid_move(cell(4,3), cell(4,4))). % down
neg(valid_move(cell(1,1), cell(0,1))). % left
neg(valid_move(cell(5,2), cell(5,1))). % up
```

The tuning of the bias settings was an iterative process for me, here are some of the major highlights:

During the learnig in Popper I've encountered the following issue

```
pyswip.prolog.PrologError:
Caused by: 'pos_covered(Xs)'.
Returned: 'error(instantiation_error, context(:(system, /(succ, 2)), _6180))'.
```

This is due to the fact that Popper is trying to use the operator `succ` on some not grounded terms.

So I've introduced a workaround that checks if the term is grounded before using the operator `succ`:

```
safe_succ(A,B) :- integer(A), succ(A,B).
safe_succ(A,B) :- integer(B), succ(A,B).
```

But then some similar error happend

```
pyswip.prolog.PrologError:
Caused by: 'pos_covered(Xs)'.
Returned: 'error(instantiation_error, context(:(system, /(>, 2)), _6180))'.
```

Here again Popper is using the operator `>` on some not grounded terms.

So I defined the types in the file `bias.pl` as follows:

```prolog
type(adjacent,(cell, cell)).
type(valid_move,(cell, cell)).
type(legal_position,(cell,)).
```

In this way I get in a non determisitic way this solution `valid_move(A,B):- legal_position(B),adjacent(C,A),legal_position(C).` or the previous error.

Having a proper typing for numbers the previous error would not occur and the hypothesis space will be reduced in case of non number variables.

So I decided to define the direction of my predicates as follows:

```
direction(adjacent,(out,out)).
direction(valid_move,(in,out)).
direction(legal_position,(in,)).
```

Now I get a solution that covers my examples but does not make much sense.

```prolog
valid_move(A,B) :-
  adjacent(C,B),
  legal_position(C),
  legal_position(B),
  legal_position(A).
```

I had to tune the hyperparameters for Popper to get the desired result.

```prolog
max_vars(2).
% or
max_body(2).
```

## Experiments

All the results shown below have **precision=1** and **recall=1**, the generated clauses cover all the examples.

### Metalog

#### Suggested metarules

```prolog
metarule([P,Q], [P,A,B], [[Q,A,B]]). % identity
metarule([P,Q], [P,A,B], [[Q,B,A]]). % inverse
metarule([P,Q,R], [P,A,B], [[Q,A],[R,A,B]]). % precon
metarule([P,Q,R], [P,A,B], [[Q,A,B],[R,B]]). % postcon
metarule([P,Q,R], [P,A,B], [[Q,A,C],[R,C,B]]). % chain

?- time(a).
% learning valid_move/2
% clauses: 1
% clauses: 2
% clauses: 3
valid_move(A,B) :-
  adjacent(A,B),
  legal_position(B).

valid_move(A,B) :-
  valid_move_1(B,A).

valid_move_1(A,B) :-
  legal_position(A),
  adjacent(A,B).

% 119,312 inferences, 0.022 CPU in 0.023 seconds (95% CPU, 5487880 Lips)
```

#### inverse + postcon

```prolog
metarule([P,Q], [P,A,B], [[Q,B,A]]). % inverse
metarule([P,Q,R], [P,A,B], [[Q,A,B],[R,B]]). % postcon

?- time(a).
% learning valid_move/2
% clauses: 1
% clauses: 2
% clauses: 3

valid_move(A,B) :-
  adjacent(A,B),
  legal_position(B).

valid_move(A,B) :-
  valid_move_1(A,B),
  legal_position(B).

valid_move_1(A,B) :-
  adjacent(B,A).

% 56,326 inferences, 0.007 CPU in 0.008 seconds (90% CPU, 7912066 Lips)
```

#### postcon + inverse_postcon

```prolog
metarule([P,Q,R], [P,A,B], [[Q,A,B],[R,B]]). % postcon
metarule([P,Q,R], [P,A,B], [[Q,B,A],[R,B]]). % inverse_postcon

?- time(a).
% learning valid_move/2
% clauses: 1
% clauses: 2

valid_move(A,B) :-
  adjacent(A,B),
  legal_position(B).

valid_move(A,B) :-
  adjacent(B,A),
  legal_position(B).

% 50,103 inferences, 0.012 CPU in 0.013 seconds (92% CPU, 4207154 Lips)
```

#### double postcon

In this experiment I wanted to replace `body_pred(legal_position/1)` with `body_pred(inside_grid/1)` and `body_pred(is_free/1)` so I forged the following metarules:

```prolog
metarule([P,Q,R,S], [P,A,B], [[Q,A,B],[R,B],[S,B]]). % double post-con
metarule([P,Q,R,S], [P,A,B], [[Q,B,A],[R,B],[S,B]]). % inverse double post-con

?- time(a).
% learning valid_move/2
% clauses: 1
% clauses: 2

valid_move(A,B) :-
  adjacent(A,B),
  inside_grid(B),
  is_free(B).

valid_move(A,B) :-
  adjacent(B,A),
  inside_grid(B),
  is_free(B).

% 52,509 inferences, 0.013 CPU in 0.015 seconds (91% CPU, 3948936 Lips)
```

#### Popper

##### Learning valid_move/2

With the same setup described in the Implementation section, Popper learned the following clauses:

```prolog
% Precision:1.00 Recall:1.00 TP:4 FN:0 TN:5 FP:0 Size:6

valid_move(A,B) :-
  adjacent(B,A),
  legal_position(B).

valid_move(A,B) :-
  adjacent(A,B),
  legal_position(B).

% Total programs: 7
% Combine:
%         Called: 2 times          Total: 0.00     Mean: 0.001     Max: 0.001
% Constrain:
%         Called: 7 times          Total: 0.00     Mean: 0.000     Max: 0.001
% Test:
%         Called: 7 times          Total: 0.00     Mean: 0.000     Max: 0.000
% Generate:
%         Called: 7 times          Total: 0.00     Mean: 0.000     Max: 0.000
% Total operation time: 0.00s
% Total execution time: 0.03s
```

##### Learning valid_move/2 with inside_grid/1 and is_free/1

To learn valid move without the predicate `legal_position/1` I made the following changes:

```prolog
% bias.pl

body_pred(is_free,1).
body_pred(inside_grid,1).

% ... also defined types and directions

max_vars(2). % this avoids the usage of different variables from A and B
max_body(3). % increases the number of clauses to 3

% exs.pl
% added the folowing negative example to cover the case where the cell is outside the grid
neg(valid_move(cell(5,5), cell(6,5))).  % up
```

Solution

```prolog
% Precision:1.00 Recall:1.00 TP:4 FN:0 TN:5 FP:0 Size:8

valid_move(A,B) :-
  adjacent(B,A),
  inside_grid(B),
  is_free(B).

valid_move(A,B) :-
  adjacent(A,B),
  inside_grid(B),
  is_free(B).

% Total programs: 23
% Test:
%         Called: 23 times         Total: 0.00     Mean: 0.000     Max: 0.000
% Generate:
%         Called: 23 times         Total: 0.00     Mean: 0.000     Max: 0.000
% Combine:
%         Called: 2 times          Total: 0.00     Mean: 0.001     Max: 0.001
% Constrain:
%         Called: 23 times         Total: 0.00     Mean: 0.000     Max: 0.001
% Total operation time: 0.01s
% Total execution time: 0.03s
```

### Bonus: `adajcent` in hyper

In this experiment I wanted to try Hyper, in particular I wanted to try terms refinement. So I tried to learn the `adjacent/2` predicate.

This is how I setup the task:

```prolog
% Parameter settings
max_proof_length( 2).
max_clauses(2).
max_clause_length(2).

safe_succ(A,B) :- integer(A), succ(A,B).
safe_succ(A,B) :- integer(B), succ(A,B).

% Background literals
backliteral(safe_succ(A, B), [A:integer], [ B:integer]).

% Refinement of terms
term( cell, c(X, Y), [X:integer, Y:integer]).

% background predicates
prolog_predicate(safe_succ(_,_)).

start_clause( [adjacent( C1, C2)] / [ C1:cell, C2:cell] ).

% ... 4 positive examples and 3 negative examples
```

Defining the task in this way does not leads to a solution, hyper does not use grounded terms during the learning, so I had to change `safe_succ/2` as follows.

```prolog
domain(L) :- numlist(1,5,L).
safe_succ(X,Y):- domain(L), member(X, L), member(Y, L), (Y is X+1; Y is X-1).
```

Now it leads to this solution:

```prolog
% Hypotheses generated: 23694
% Hypotheses refined:   4479
% To be refined:        7373

adjacent(c(A,B),c(A,C)):-
  safe_succ(B,C).

adjacent(c(A,B),c(C,B)):-
  safe_succ(A,C).

% 21,909,803 inferences, 1.744 CPU in 1.804 seconds (97% CPU, 12559972 Lips)
```

Hyper is a powerful tool to learn predicates, but it is not easy to use, the documentation is lacking and is hard to find advanced examples.

I really like the type refinement that it provides, other tools should implment it too.

## Performance

All the times are in seconds, an Imac with M1 processor has been used for the tests.

All the solution has precision 1.00 and recall 1.00.

|              | Metagol | Popper            | Hyper |
| ------------ | ------- | ----------------- | ----- |
| valid_move   | 0.013   | op 0.00 / ex 0.03 | NA    |
| valid_move\* | 0.015   | op 0.01 / ex 0.03 | NA    |
| adjacent     | NA      | NA                | 1.804 |

(\*) referes to `valid_move/2` with `inside_grid/1` and `is_free/1`

### Performance with different number of examples

In this comparison we learn `valid_move/2` with the predicate `legal_position/1` in the body.

### Minimal number of examples

#### Metagol

2 positive examples

```prolog
valid_move(cell(3,1), cell(4,1)), % right
valid_move(cell(4,3), cell(3,3)) % left
```

Popper
2 positive examples, 2 negative examples

```prolog
pos(valid_move(cell(3,1), cell(4,1))). % right
pos(valid_move(cell(4,3), cell(3,3))). % left

neg(valid_move(cell(4,1), cell(5,1))). % right
neg(valid_move(cell(1,1), cell(0,1))). % left
```

I run 5 times the task with different number of examples and I get the following times:

|               | Metagol                    | Popper               |
| ------------- | -------------------------- | -------------------- |
| minimal       | 49,666 inferences / 0.013s | op 0.005s / ex 0.02s |
| 4 pos + 4 neg | 50,106 inferences / 0.014s | op 0.01s / ex 0.03s  |
| 8 pos + 8 neg | 50,740 inferences / 0.015s | op 0.01s / ex 0.03s  |

From this experiment I can say that both learning times increase with the number of examples, Metagol seems to increase more than Popper.

## Conclusions

The two systems has a completely different apporach on the bias of the language.

In Metagol the metarules are really powerful. Using the feewer rules the performance of the system is better. However, choosing the right rules is not a trivial task, when there is no initial idea on how the final result should look like.

In Popper, was possible to exploit his more general approach as it doesn't require metarules. Nevertheless, a lot of attention is required
when defining the examples and the hyperparameters.

Both systems works well, It is reccomended to choose one or the other, depending on the problem.

Personally, I think that the Popper system is the best, because it is more general and it is more flexible.
