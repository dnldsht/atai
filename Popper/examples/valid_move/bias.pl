max_vars(2).
max_body(3).

% enable_pi. % does not invet predicates for this task

head_pred(valid_move,2).
body_pred(adjacent,2).
%body_pred(legal_position,1).
body_pred(is_free,1).
body_pred(inside_grid,1).


type(adjacent,(cell, cell)).
type(valid_move,(cell, cell)).
%type(legal_position,(cell,)).
type(is_free,(cell,)).
type(inside_grid,(cell,)).

direction(adjacent,(out,out)).
direction(valid_move,(in,out)).
%direction(legal_position,(in,)).
direction(is_free,(in,)).
direction(inside_grid,(in,)).
