%% background knowledge
% defines grid NxN
grid(5,5).

%   | 1 | 2 | 3 | 4 | 5 |
%   ---------------------
% 1 |   |   |   |   | x |
% 2 | x |   | x |   |   |
% 3 |   |   |   |   |   |
% 4 | x | x | x | x |   |
% 5 | x |   |   |   |   |

% obstacles
obstacle(cell(5,1)).
obstacle(cell(1,2)).
obstacle(cell(3,2)).
obstacle(cell(1,4)).
obstacle(cell(2,4)).
obstacle(cell(3,4)).
obstacle(cell(4,4)).
obstacle(cell(1,5)).

safe_succ(A,B) :- integer(A), succ(A,B).
safe_succ(A,B) :- integer(B), succ(A,B).

adjacent(cell(X,Y),cell(X,Y1)) :-
    safe_succ(Y1, Y).

adjacent(cell(X,Y),cell(X1,Y)) :-
    safe_succ(X, X1).     

inside_grid(cell(X,Y)) :-
    grid(Width,Height),
    X > 0, 
    X =< Width,
    Y > 0, 
    Y =< Height.

is_free(CELL) :- \+ obstacle(CELL).

legal_position(CELL) :-
    inside_grid(CELL),
    is_free(CELL).
