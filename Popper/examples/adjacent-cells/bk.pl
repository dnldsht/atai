% defined cells where you can walk
c(1,1). 
c(1,2).         c(3,2).         c(5,2). c(6,2).
        c(2,3). c(3,3).     
        c(2,4).         c(4,4).                 
c(1,5).                 c(4,5). c(5,5).
c(1,6).                 c(4,6). 

my_succ(A,B):-
  integer(A),
  succ(A,B).

my_succ(A,B):-
  integer(B),
  succ(A,B).