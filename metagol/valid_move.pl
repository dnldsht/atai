:- use_module('metagol.pl').

%% metagol settings
%metagol:max_clauses(4).

%% tell metagol to use the BK
body_pred(adjacent/2).
%body_pred(legal_position/1).
body_pred(inside_grid/1).
body_pred(is_free/1).


%% metarules
%metarule([P,Q], [P,A,B], [[Q,A,B]]). % identity
%metarule([P,Q], [P,A,B], [[Q,B,A]]). % inverse
%metarule([P,Q,R], [P,A,B], [[Q,A],[R,A,B]]). % precon
%metarule([P,Q,R], [P,A,B], [[Q,A,B],[R,B]]). % postcon
%metarule([P,Q,R], [P,A,B], [[Q,A,C],[R,C,B]]). % chain
%metarule([P,Q,R], [P,A,B], [[Q,B,A],[R,B]]). % inverse_postcon


metarule([P,Q,R,S], [P,A,B], [[Q,A,B],[R,B],[S,B]]). % double post-con
metarule([P,Q,R,S], [P,A,B], [[Q,B,A],[R,B],[S,B]]).


%% background knowledge
% defines grid NxM
grid(5,5).

%   | 1 | 2 | 3 | 4 | 5 |
%   ---------------------
% 1 |   |   |   |   | x |
% 2 | x |   | x |   |   |
% 3 |   |   |   |   |   |
% 4 | x | x | x | x |   |
% 5 | x |   |   |   |   |

% obstacles
obstacle(cell(5,1)).
obstacle(cell(1,2)).
obstacle(cell(3,2)).
obstacle(cell(1,4)).
obstacle(cell(2,4)).
obstacle(cell(3,4)).
obstacle(cell(4,4)).
obstacle(cell(1,5)).

safe_succ(A,B) :- integer(A), succ(A,B).
safe_succ(A,B) :- integer(B), succ(A,B).

adjacent(cell(X,Y),cell(X,Y1)) :-
    safe_succ(Y1, Y).

adjacent(cell(X,Y),cell(X1,Y)) :-
    safe_succ(X, X1).     

inside_grid(cell(X,Y)) :-
    integer(X), integer(Y),
    grid(Width,Height),
    X > 0, 
    X =< Width,
    Y > 0, 
    Y =< Height.

is_free(CELL) :- \+ obstacle(CELL).

legal_position(CELL) :-
    inside_grid(CELL),
    is_free(CELL).


%% learning task
learn :-
    Pos = [
        valid_move(cell(3,1), cell(4,1)), % right
        valid_move(cell(2,2), cell(2,3)), % down
        valid_move(cell(4,3), cell(3,3)), % left
        valid_move(cell(2,2), cell(2,1))  % up
    ],

    Neg = [
        valid_move(cell(4,1), cell(5,1)), % right 
        valid_move(cell(4,3), cell(4,4)), % down
        valid_move(cell(1,1), cell(0,1)), % left
        valid_move(cell(5,2), cell(5,1)) % up
        % valid_move(cell(1,1), cell(1,1))  % not same
    ],
    learn(Pos, Neg). 