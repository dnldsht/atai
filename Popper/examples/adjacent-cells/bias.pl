%% f(A,B) :- empty(A),zero(B).
%% f(A,B) :- tail(A,D),f(D,C),succ(C,B).
%% python3 popper.py examples/length  0.28s user 0.03s system 99% cpu 0.314 total

max_vars(4).
max_body(4).
max_clauses(4).
% enable_recursion.

head_pred(adjacent,4).
body_pred(c,2).
body_pred(my_succ,2).




% type(adjacent,(integer,integer, integer, integer)).
% type(c,(integer,integer)).
% type(my_succ,(integer,integer)).

% direction(adjacent,(in,in,out,out)).
% direction(c,(in,in)).
% direction(my_succ,(out,out)).
