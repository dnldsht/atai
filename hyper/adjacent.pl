
% Parameter settings
max_proof_length( 2).
max_clauses(2).     
max_clause_length(2).

% safe_succ(A,B) :- integer(A), succ(A,B).
% safe_succ(A,B) :- integer(B), succ(A,B).

domain(L) :- numlist(1,5,L). 
safe_succ(X,Y):- domain(L), member(X, L), member(Y, L), (Y is X+1; Y is X-1).

% Background literals
backliteral(safe_succ(A, B), [A:integer], [ B:integer]).
 
% Refinement of terms
term( cell, c(X, Y), [X:integer, Y:integer]).
		
% background predicates
prolog_predicate(safe_succ(_,_)).  

start_clause( [adjacent( C1, C2)] / [ C1:cell, C2:cell] ).  
  
ex( adjacent( c(3,3), c(2,3))). % left
ex( adjacent( c(4,5), c(4,4))). % up
ex( adjacent( c(3,1), c(4,1))). % right
ex( adjacent( c(1,3), c(1,4))). % down


nex( adjacent( c(1,2), c(1,2))).
nex( adjacent( c(3,3), c(2,4))).
nex( adjacent( c(3,3), c(3,1))).

learn :- induce(X), show_hyp(X).